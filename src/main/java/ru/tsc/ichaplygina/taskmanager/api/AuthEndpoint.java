package ru.tsc.ichaplygina.taskmanager.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.tsc.ichaplygina.taskmanager.model.Result;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/auth")
public interface AuthEndpoint {

    @WebMethod
    @GetMapping(value = "/login", produces = "application/json")
    Result login(@RequestParam("username") @WebParam(name = "username") final String username,
                 @RequestParam("password") @WebParam(name = "password") final String password);

    @WebMethod
    @GetMapping(value = "/logout", produces = "application/json")
    Result logout();

}
