package ru.tsc.ichaplygina.taskmanager.client.soap;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.TasksEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class TasksSoapEndpointClient {

    public static TasksEndpoint getInstance(@NotNull final String baseURL) throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/ws/TasksEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "TasksEndpointImplService";
        @NotNull final String ns = "http://endpoint.taskmanager.ichaplygina.tsc.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final TasksEndpoint result = Service.create(url, name).getPort(TasksEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
