package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.ichaplygina.taskmanager.enumerated.RoleType;
import ru.tsc.ichaplygina.taskmanager.model.Role;
import ru.tsc.ichaplygina.taskmanager.model.User;
import ru.tsc.ichaplygina.taskmanager.repository.UserRepository;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;

@Service
public class UserService {

    @NotNull
    @Autowired
    private UserRepository repository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMIN);
        initUser("user", "user", RoleType.USER);
    }

    private void initUser(@NotNull final String login, @NotNull final String password, @NotNull final RoleType roleType) {
        @Nullable final User user = repository.findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    @Transactional
    public void createUser(@NotNull final String login, @NotNull final String password, @NotNull final RoleType roleType) {
        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        @NotNull final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        repository.save(user);
    }

    public List<User> findAll() {
        return repository.findAll();
    }

    public User findById(@NotNull final String id) {
        return repository.findById(id).orElse(null);
    }

    public User findByLogin(@NotNull final String login) {
        return repository.findByLogin(login);
    }

    @Transactional
    public void remove(List<User> users) {
        repository.deleteAll(users);
    }

    @Transactional
    public void removeAll() {
        repository.deleteAll();
    }

    @Transactional
    public void removeById(@NotNull final String id) {
        repository.deleteById(id);
    }

    @Nullable
    @Transactional
    public User write(User user) {
        return repository.save(user);
    }

    @NotNull
    @Transactional
    public List<User> write(@NotNull final List<User> users) {
        return repository.saveAll(users);
    }

}
