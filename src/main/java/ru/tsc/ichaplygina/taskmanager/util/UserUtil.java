package ru.tsc.ichaplygina.taskmanager.util;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.tsc.ichaplygina.taskmanager.exception.AccessDeniedException;
import ru.tsc.ichaplygina.taskmanager.model.CustomUser;

public class UserUtil {

    public static String getUserId() {
        @NotNull final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        @NotNull final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException();
        if (!(principal instanceof CustomUser)) throw new AccessDeniedException();
        final CustomUser customUser = (CustomUser) principal;
        return customUser.getUserId();

    }

}
