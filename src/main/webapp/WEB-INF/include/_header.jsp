<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Task Manager</title>
</head>
<style>
    h1 {
        font-size: 1.6em;
    }

    a {
        color: darkblue;
    }
</style>

<body>
<h1>Task Manager</h1>
<a href="/projects">Projects</a>
<a href="/tasks">Tasks</a>
<sec:authorize access="isAuthenticated()">
<a href="/logout">Logout</a>
</sec:authorize>
<sec:authorize access="!isAuthenticated()">
<a href="/login">Login</a>
</sec:authorize>


