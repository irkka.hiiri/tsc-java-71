package ru.tsc.ichaplygina.taskmanager.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.ichaplygina.taskmanager.config.DatabaseConfiguration;
import ru.tsc.ichaplygina.taskmanager.config.WebApplicationConfiguration;
import ru.tsc.ichaplygina.taskmanager.marker.UnitCategory;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.service.TaskService;
import ru.tsc.ichaplygina.taskmanager.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DatabaseConfiguration.class})
public class TaskControllerTest {

    @Autowired
    @NotNull
    private TaskService taskService;

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    @NotNull
    private WebApplicationContext wac;

    @NotNull
    private final Task task1 = new Task("Test Task 1");

    @NotNull
    private final Task task2 = new Task("Test Task 2");

    @NotNull
    private final Task task3 = new Task("Test Task 3");

    @NotNull
    private final Task task4 = new Task("Test Task 4");

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskService.write(UserUtil.getUserId(), task1);
        taskService.write(UserUtil.getUserId(), task2);
    }

    @After
    public void clean() {
        taskService.removeAll(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findAllTest() {
        @NotNull final String url = "/tasks";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void createTest() {
        @NotNull final String url = "/task/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @NotNull final List<Task> tasks = taskService.findAll(UserUtil.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(3, tasks.size());

    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteTest() {
        @NotNull final String url = "/task/delete/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertNull(taskService.findById(UserUtil.getUserId(), task1.getId()));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void editTest() {
        @NotNull final String url = "/task/edit/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

}
