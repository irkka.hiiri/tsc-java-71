package ru.tsc.ichaplygina.taskmanager.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.ichaplygina.taskmanager.config.DatabaseConfiguration;
import ru.tsc.ichaplygina.taskmanager.marker.UnitCategory;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.repository.ProjectRepository;
import ru.tsc.ichaplygina.taskmanager.util.UserUtil;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class ProjectRepositoryTest {

    @NotNull
    private final Project project1 = new Project("Test Project 1");

    @NotNull
    private final Project project2 = new Project("Test Project 2");

    @NotNull
    private final Project project3 = new Project("Test Project 3");

    @NotNull
    private final Project project4 = new Project("Test Project 4");

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        project3.setUserId(UserUtil.getUserId());
        project4.setUserId(UserUtil.getUserId());
        projectRepository.save(project1);
        projectRepository.save(project2);
    }

    @After
    public void clean() {
        projectRepository.deleteAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findAllTest() {
        Assert.assertEquals(2, projectRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findByUserIdAndIdTest() {
        Assert.assertNotNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), project1.getId()));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteAllByUserIdTest() {
        projectRepository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, projectRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteByUserIdAndIdTest() {
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), project1.getId());
        Assert.assertNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), project1.getId()));
    }

}
