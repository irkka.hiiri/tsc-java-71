package ru.tsc.ichaplygina.taskmanager.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.ichaplygina.taskmanager.config.DatabaseConfiguration;
import ru.tsc.ichaplygina.taskmanager.marker.UnitCategory;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.service.ProjectService;
import ru.tsc.ichaplygina.taskmanager.util.UserUtil;

import java.util.ArrayList;
import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class ProjectServiceTest {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;


    @NotNull
    private final Project project1 = new Project("Test Project 1");

    @NotNull
    private final Project project2 = new Project("Test Project 2");

    @NotNull
    private final Project project3 = new Project("Test Project 3");

    @NotNull
    private final Project project4 = new Project("Test Project 4");

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectService.write(UserUtil.getUserId(), project1);
        projectService.write(UserUtil.getUserId(), project2);
    }

    @After
    public void clean() {
        projectService.removeAll(UserUtil.getUserId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAll() {
        Assert.assertEquals(2, projectService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findById() {
        Assert.assertNotNull(projectService.findById(UserUtil.getUserId(), project1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void remove() {
        @NotNull final List<Project> projects = new ArrayList<>();
        projects.add(project1);
        projectService.remove(UserUtil.getUserId(), projects);
        Assert.assertNull(projectService.findById(UserUtil.getUserId(), project1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeAll() {
        projectService.removeAll(UserUtil.getUserId());
        Assert.assertEquals(0, projectService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeById() {
        projectService.removeById(UserUtil.getUserId(), project1.getId());
        Assert.assertNull(projectService.findById(UserUtil.getUserId(), project1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void write() {
        projectService.write(UserUtil.getUserId(), project3);
        Assert.assertNotNull(projectService.findById(UserUtil.getUserId(), project3.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void writeList() {
        @NotNull final List<Project> projects = new ArrayList<>();
        projects.add(project3);
        projects.add(project4);
        projectService.write(UserUtil.getUserId(), projects);
        Assert.assertNotNull(projectService.findById(UserUtil.getUserId(), project3.getId()));
        Assert.assertNotNull(projectService.findById(UserUtil.getUserId(), project4.getId()));
    }

}
