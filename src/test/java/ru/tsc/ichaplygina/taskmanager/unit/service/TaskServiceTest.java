package ru.tsc.ichaplygina.taskmanager.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.ichaplygina.taskmanager.config.DatabaseConfiguration;
import ru.tsc.ichaplygina.taskmanager.marker.UnitCategory;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.service.TaskService;
import ru.tsc.ichaplygina.taskmanager.util.UserUtil;

import java.util.ArrayList;
import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class TaskServiceTest {

    @NotNull
    @Autowired
    private TaskService taskService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;


    @NotNull
    private final Task task1 = new Task("Test Task 1");

    @NotNull
    private final Task task2 = new Task("Test Task 2");

    @NotNull
    private final Task task3 = new Task("Test Task 3");

    @NotNull
    private final Task task4 = new Task("Test Task 4");

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskService.write(UserUtil.getUserId(), task1);
        taskService.write(UserUtil.getUserId(), task2);
    }

    @After
    public void clean() {
        taskService.removeAll(UserUtil.getUserId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAll() {
        Assert.assertEquals(2, taskService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findById() {
        Assert.assertNotNull(taskService.findById(UserUtil.getUserId(), task1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void remove() {
        @NotNull final List<Task> tasks = new ArrayList<>();
        tasks.add(task1);
        taskService.remove(UserUtil.getUserId(), tasks);
        Assert.assertNull(taskService.findById(UserUtil.getUserId(), task1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeAll() {
        taskService.removeAll(UserUtil.getUserId());
        Assert.assertEquals(0, taskService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeById() {
        taskService.removeById(UserUtil.getUserId(), task1.getId());
        Assert.assertNull(taskService.findById(UserUtil.getUserId(), task1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void write() {
        taskService.write(UserUtil.getUserId(), task3);
        Assert.assertNotNull(taskService.findById(UserUtil.getUserId(), task3.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void writeList() {
        @NotNull final List<Task> tasks = new ArrayList<>();
        tasks.add(task3);
        tasks.add(task4);
        taskService.write(UserUtil.getUserId(), tasks);
        Assert.assertNotNull(taskService.findById(UserUtil.getUserId(), task3.getId()));
        Assert.assertNotNull(taskService.findById(UserUtil.getUserId(), task4.getId()));
    }

}
